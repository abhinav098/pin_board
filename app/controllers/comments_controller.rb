class CommentsController < ApplicationController

 def create
    @pin= Pin.find(params[:pin_id])
    @comment=@pin.comments.create(params[:comment].permit(:name, :comment))
    redirect_to pin_path(@pin, errors: @comment.errors.messages)
  end

  def edit
    @pin= Pin.find(params[:pin_id])
    @comment=Comment.find(params[:id])
  end

  def update
    @pin= Pin.find(params[:pin_id])
    @comment=@pin.comments.find(params[:id])
    if @comment.update(params[:comment].permit(:name, :comment))
      redirect_to @comment.pin if @comment.save
    else
      redirect_to :back
    end
  end

  def destroy
    @pin= Pin.find(params[:pin_id])
    @comment=@pin.comments.find(params[:id])
    @comment.destroy
    redirect_to :back
  end

end
