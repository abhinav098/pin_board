class PinsController < ApplicationController
before_action :find_pin, only: [:show, :edit, :update, :destroy,:upvote]
before_action :authenticate_user!, except: [:index, :show]
  def index
    @pins=Pin.all.order("created_at DESC")
  end

  def new
    @pin=current_user.pins.build
  end

  def create
    @pin=current_user.pins.build(pin_params)
    if @pin.save
       flash[:success] = "Pin Successfully created!"
      redirect_to @pin
    else
      render 'new'
    end
  end

  def show
    @comment = Comment.new
    if params[:errors]
      params[:errors].each do |name, message|
        message.each do |msg|
          @comment.errors.add(name.to_sym, msg)
        end
      end
    end
  end

  def update
    if @pin.update(pin_params)
       flash[:success] = "Pin Successfully Updated!"
      redirect_to @pin
    else
      render 'edit'
    end
  end

  def destroy
    @pin.destroy
    flash[:danger] = "Pin Successfully Deleted!"
    redirect_to root_path
  end

  def upvote
    @pin.upvote_by current_user
    redirect_to :back
  end

private
  def pin_params
    params.require(:pin).permit(:title, :description, :image)
  end

  def find_pin
    @pin=Pin.find(params[:id])
  end

end


