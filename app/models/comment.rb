class Comment < ActiveRecord::Base
  validates :name, presence:true ,length: {minimum: 3 }
  validates :comment, presence:true ,length: {minimum: 3}
  belongs_to :user
  belongs_to :pin
end
